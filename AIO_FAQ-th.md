AIO FAQ:
--------
**Q: วิธีค้นหา**
> A: กด "Ctrl+F" แล้วป้อนคำค้น

**Q: ใช้ AIO อย่างไร?**
> A: คลายไฟล์ ดับเบิ้ลคลิก choose.cmd
- กดปุ่มที่คีย์บอร์ด "A"ถึง"Z" และ "4"ถึง"6" เมื่อจะติดตั้ง กดอีกเลือกอีกรอบถ้าต้องการยกเลิก
- กด "9" อ่านช่วยเหลือ กด "1" หรือ "2" เพื่อสลับ install กับ uninstall
- กด "8" ติดตั้งทุกปลั๊กอิน
- กด "0" เพื่อให้ทำไฟล์ติดตั้งและเขียนลง USB flash drive

**Q:ต้องรอนานเท่าไหร่ โปรแกรมจึงเริ่มติดตั้ง นับจากใส่ USB ในรถ?**
> A: ใช้เวลาตั้งแต่ 30วินาที ถึง 2นาที

**Q:ขณะติดตั้งใช้เวลานานเท่าไหร่**
> A: ไม่เกิน 8นาที

**Q:ใช้เวลาถอนติดตั้งนานเท่าไหร่?**
> A:ไม่เกิน 5นาที ถ้าติดตั้งปลั๊กอินทุกตัว

**Q: Video Player รองรับ video ชนิดใดบ้าง?**
> A: Video: MP4, AVI, FLV, WMV;  Audio: Stereo MPEG-4 AAC audio codec

**Q: จำเป็นต้องถอนติดตั้ง(uninstall)ก่อนติดตั้งใหม่หรือไม่?**
> A: ไม่จำเป็น, ท่านติดตั้งได้เลยเพราะโปรแกรมติดตั้งจะลบปลั๊กอินเก่าก่อนติดตั้งใหม่ ทุกปลั๊กอินที่มีไม่ต้องuninstall ติดตั้งทับได้เลย ยกเว้นปลั๊กอิน [A] Touchscreen

**Q: ปลั๊กอินเก่าที่เคยติดตั้ง จะถูกลบเมื่อติดตั้ง AIOเวอร์ชั่นใหม่โดยไม่ได้เลือกปลั๊กอินเก่าหรือไม่?**
> A: ไม่ ติดตั้งปลั๊กอินใหม่เท่านั้น ไม่ทำอะไรกับปลั๊กอินเก่า

**Q: เมื่อต้องการลบปลั๊กอินต้องใช้ AIO เวอร์ชั่นเดียวกันหรือไม่?**
> A: ไม่จำเป็น, ท่านใช้ AIO เวอร์ชั่นล่าสุด uninstall เวอร์ชั่นเก่ากว่าได้

**Q: ถ้าต้องการกลับไปเหมือนเดิมก่อนติดตั้ง แต่จำไม่ได้ว่าติดตั้งอะไรไปบ้าง?**
> A: ทำได้ ไม่เกิดปัญหากับการ uninstall เมื่อเลือกปลั๊กอินที่ไม่ได้ติดตั้ง

**Q: เข็มทิศกลับทิศใน speedometer ต้องไขอย่างไร?**
> A: ใส่ SD Card NAV แล้วติดตั้ง speedometer ใหม่
> ถ้ายังไม่ได้ SSH เข้าไปแก้ /jci/opera/opera_dir/userjs/speedometer-startup.js
> หาตัวแปร noNavSD แก้จาก true เป็น false
```javascript
var noNavSD = false;
```
cr: [sathienh](http://www.mazda2thailand.com/index.php/topic,2786.msg75795.html#msg75795)

**Q: ปัจจุบันมีเฟิร์มแวร์เวอร์ชั่นใดบ้าง?**
> A: อ่าน choose\docs\FW info.txt

**Q: มีปลั๊กอิน backup /jci แต่ทำไมไม่มีปลั๊กอิน restore?**
> A: It's not necessary,  ทุกปลั๊กอินจะสำรองไฟล์ต้นฉบับเป็น *.org เมื่อถอนติดตั้งจะนำไฟล์กลับมา ไฟล์ที่แก้ไขนั้นนับเป็นไฟล์ส่วนในจากไฟล์ทั้งหมดใน /jci

**Q: AIO รองรับเฟิร์มแวร์เวอร์ชั่นใดบ้าง?**
> A: อ่าน [README.md](https://github.com/khantaena/AIO---All-in-one-tweaks/blob/master/README.md) หัวข้อ WHICH FW VERSIONS ARE SUPPORTED รายการเฟิร์มแวร์ที่รองรับทั้งหมดอยู่ในนั้น

**Q: รีบูท MZD ทำอย่างไร?**
> A: กดปุ่ม NAV+MUTE ค้างไว้ 10 วินาที.

**Q: background ต้นฉบับอยู่ที่ไหน?**
> A: ใน \choose\more_background-images

**Q: แปลงภาพอย่างไรเพื่อใช้กับ Background Rotator?**
> A: เข้าเว็บ: [PhotoJoiner.net](http://old.photojoiner.net/)
   1. เลือก "Select Photos" เลือกรูป 10รูป (ดูใน \more_background-images)
   2. กำหนด "Style" เลือก "แนวตั้ง" เสร็จแล้วเลือก "Join Photos" คลิกขวาเลือก "Save Image as" เพื่อ save ไฟล์
   3. ใช้โปรแกรมแปลงไฟล์จาก *.jpg เป็น *.png ตั้งชื่อไฟล์เป็น background.png
   4. นำไฟล์ background.png บันทึกลง USB Flash Drive ไว้ที่

   ```cmd
   \choose\config_all\BackgroundRotator\jci\gui\common\images
   ```

   หากต้องการใช้ภาพ มากกว่าหรือน้อยกว่า 10ภาพ และเปลี่ยนความเร็วการแสดงผล ทำได้ด้วยแก้ไขไฟล์ config/BackgroundRotator/common.css บรรทัดที่ 25 เช่น เมื่อต้องการใช้ภาพ 20 ภาพ โชว์ภาพละ 10 วินาที:

จากเดิม
   ```css
   animation: slide 600s steps(10,end) infinite;
   ```
เป็น
   ```css
   animation: slide 200s steps(20,end) infinite;
   ```

**Q: เมื่ออยู่หน้าต่าง Entertainment ใช้งาน pause-on-mute ได้ปกติ แต่เมื่อไปที่หน้าต่างอื่น กลายเป็นปิดเสียงไม่หยุดเพลง**
> A: เป็นการทำงานปกติของปลั๊กอิน ปลั๊กอิน pause-on-mute ทำงานได้เฉพาะเมื่ออยู่ที่หน้าต่าง Entertainment ใช้ไม่ได้กับ app อื่น

**Q: อยากได้ข้อมูล speedometer.**
> A: อ่าน speedometer_readme.txt

**Q: ติดตั้ง AIO จาก SD Card ได้หรือไม่?**
> A: ไม่ได้

**Q: videos player ไม่มีเสียง แก้ไขอย่างไร?**
> A: อ่าน [#291](http://www.mazda2thailand.com/index.php/topic,2786.msg87453.html#msg87453) กด Pause เพลงก่อน แล้วปรับตัวเล่นเสียงไปที่ USB จากนั้นก็เข้า video player เล่นไฟล์ตามปกติ  cr:[zixes](http://www.mazda2thailand.com/index.php?action=profile;u=7216)

**Q: ใช้ videos player ไม่ได้ ต้องทำอย่างไร?**
> A: ทำขั้นตอนต่อไปนี้
 * อ่าน [#164](http://www.mazda2thailand.com/index.php/topic,2786.msg77118.html#msg77118) ติดตั้งปลั๊กอิน [4] อย่างเดียวเพียงพอ
 * ใช้ H264 video codec และ MPEG-4 AAC audio codec
 * นำไฟล์ mp3 ใส่ใน USB ไดเร็กทอรี่บนสุด เช่น D:\point1sec.mp3 ผมใช้ไฟล์ [point1sec.mp3](http://www.xamuel.com/blank-mp3-files/point1sec.mp3)
 * เก็บไฟล์หนังใน USB flash drive ในไดเร็กทอรี่แรกของไดร์ฟ ชื่อ **Movies** ขนาดตัวอักษรต้องตรง เช่น D:\Movies
 * ถ้าเล่นไฟล์ mp4 บางไฟล์ได้ แสดงว่าไฟล์ที่เล่นไม่ได้ใช้ codec ที่ videos player ไม่รองรับ

**Q: ปลั๊กอิน [S] Backup ใช้งานอย่างไร?**
> A: ปัจจุบันไม่มีปลั๊กอินกู้คืน, ข้อมูล backupเป็นไฟล์ copy จากไดเร็คทอรี่ /jci วิธีการกู้คืนด้วย uninstall (กด 2) เหมาะสมที่สุดในเวลานี้สาเหตุเพราะ มีปลั๊กอินจำนวนหนึ่งที่ไม่ได้


**Q: ถ้าเคยใช้ USB Flash drive กับเครื่อง Mac จะนำมาติดตั้ง AIO อย่างไร?**
> A: อ่าน [คำสั่งลบไฟล์ที่ไม่จำเป็น](https://www.reddit.com/r/mazda3/comments/3d5lvr/mazda_infotainment_update/ctls5ef)
```bash
cd /Volumes/MAZDA
rm -rf .fseventsd .Spotlight-V100 .Trashes
```

**Q: รายการ USB Vendor ID ที่ใช้งาน Android Auto ได้**
> A: รายการ VID ที่ใช้ AA ได้

|VID	|Vendor |
|------|---------------------------------------------|
|0x0bb4	|HTC (High Tech Computer Corp.) |
|0x22b8	|Motorola Mobility Inc.|
|0x0502	|Acer, Inc.|
|0x12d1	|Huawei Technologies Co., Ltd.|
|0x10a9	|SK Teletech Co., Ltd|
|0x19d2	|ZTE WCDMA Technologies MSM|
|0x091e	|Garmin International|
|0x2717	|               |
|0x0b05	|ASUSTek Computer, Inc.|
|0x2a45	|Meizu Corp.|
|0x17ef	|Lenovo|
|0x04e8	|Samsung Electronics Co., Ltd|
|0xfff6	|  |
|0x0fce	|Sony Ericsson Mobile Communications AB|
|0xfff5	|  |
|0x1004	|LG Electronics, Inc.|
|0x1d6b	|Linux Foundation|
|0x05c6	|Qualcomm, Inc.|
|0x2a70 | 	 |
|0x1519	|Comneon |
|0x0835	|Action Star Enterprise Co., Ltd |
|0x22d9	|OPPO|

ดู USB VID ได้ที่ [links](http://stackoverflow.com/questions/10086464/how-to-get-vendor-id-of-an-android-powered-device-via-adb) ท่านสามารถส่ง VID ให้ผู้พัฒนาเพิ่มลงในโปรแกรมได้ฟรีครับที่ [headunit](https://github.com/gartnera/headunit/issues)

**Q: ดาวน์เกรด Google Player Services จาก 9.2.56 เป็น 9.0.83 เพื่อใช้งาน AA ได้อย่างไร?**
> A: **ดาวน์เกรด 9.2.56 เป็น 9.0.83**
 1. ดาวน์โหลด [Google Play Services 9.0.83](http://www.apkmirror.com/apk/google-inc/google-play-services/google-play-services-9-0-83-release) ไว้ในโทรศัพท์
 2. จากหน้าหลักเลือกเมนู Setting->Apps เลื่อนหา Google Play Services พบแล้วกดเลือก 1ครั้ง
 3. เมื่อเข้าหน้าต่าง App info สังเกตเวอร์ชั่นเป็น 9.2.56 ให้กด DISABLE->DISABLE APP->OK
 4. สังเกตเวอร์ชั่นเปลี่ยนเป็น 8.1.86(2287566-448) เลือก ENABLE
 5. ที่โทรศัพท์เลื่อนค้นหาแอพชื่อ Downloads เพื่อเลือกติดตั้งไฟล์ com.google.android.gms_9.0.83_(240-12191110...minAPI21(arm64-v8a,armeabi-v7a)(nodpi).apk(ชื่ออาจจะแตกต่างกันได้) ที่ได้ดาวน์โหลดไว้ในขั้นตอนแรก
 6. เปิด Play Store แล้วป้อน email และ password ใหม่ (ถ้าพบปัญหาการติดตั้ง AA ให้เริ่มข้อ1ใหม่พร้อมกับเชื่อมต่อ VPN)
  6.1 ป้องกันไม่ให้ Google Play อัพเดทอัตโนมัติ
    - เปิด Google Play
    - กดปุ่มมุมบนซ้ายที่หน้าจอ Google Play
    - เลือก Settings -> Auto-update apps เปลี่ยนเป็น Do not auto-updates apps
 7. ถ้าในเครื่องมี AA อยู่แล้วเปิดใช้ได้เลย ถ้าปรากฏหน้าจอสีเทาให้กด Back(ถอยกลับ) หนึ่งครั้ง
 8. ถ้าไม่เคยติดตั้ง AA มีขั้นตอนแนะนำด้านล่างหัวข้อ Android Auto
 9. ถ้าพบปัญหาการดาวน์โหลดแอฟ ให้ต่อ VPN

**Q: ต่อ VPN ไปต่างประเทศอย่างไร?**
> A: ใช้โทรศัพท์แอนดรอยด์เชื่อมเครือข่ายต่างประเทศ
 1. ติดตั้งแอฟต่อไปนี้
  - [VPN Gate](https://play.google.com/store/apps/details?id=jp.co.sensyusya.vpngateviewer)
  - [OpenVPN](https://play.google.com/store/apps/details?id=net.openvpn.openvpn)
 2. เปิดโปรแกรม VPN Gate เลือกประเทศจากเมนูมุมบนขวา
 3. กดเลือก Connect to 123.123.123.123 (CSV) โปรแกรมจะเปิด OpenVPN
 4. เลือก ACCEPT-> Connect
 5. ถ้าเชื่อม VPN ได้จะมีไอคอน OpenVPN อยู่ที่ Top bar ถ้าเชื่อมไม่ได้ให้เปลี่ยนไอพีประเทศอื่นหรือไอพีอื่น


**Q: ใช้ SSH ในเฟิร์มแวร์ 56.00.513**
>A1: วิธีเปิดโหมด WiFi AP TOGGLE
 1. เข้าโหมด Diagnostic (Music + Favourites + Mute)
 2. กดปุ่ม DEL ค้างไว้ 20วินาที ค้นหา WiFi AP TOGGLE [JCI TEST](https://youtu.be/EMAoawiue04)สังเกตวินาทีที่ 40
 3. เปิดโน๊ตบุ๊คค้นหา AP ชื่อ "cmu ..."
 4. SSH (เช่น โปรแกรม PuTTY) ไปที่ไอพี IP 192.168.53.1 สำหรับ username และ password ท่านต้องค้นหาเองครับ

>A2: USB Tweaks :  https://github.com/mzdonline/cmu-wifiap

**Q: ขั้นตอนกู้ไฟล์ libmc_user.so ทำอย่างไร? สำหรับเฟิร์มแวร์ 56.00.100/230/240/513** 
>A: 

1. เข้าโหมด Diagnostic (Music + Favourites + Mute)
2. กดปุ่ม DEL ค้างไว้ 20วินาที 
3. กดเลข 11 แล้ว Enter 
4. กด Next ค้นหา WiFi AP TOGGLE [JCI TEST](https://youtu.be/EMAoawiue04)สังเกตวินาทีที่ 40
5. เปิดโน๊ตบุ๊คค้นหา AP ชื่อ "cmu ..." 
6. SSH (เช่น โปรแกรม PuTTY) ไปที่ไอพี IP 192.168.53.1 สำหรับ username และ password ท่านต้องค้นหาเองครับ 
7. เมื่อ connect ได้แล้วป้อนคำสั่งต่อไปนี้ 

```bash
mount -o rw,remount /
cp -a -f /jci/lib/libmc_user.so.org  /jci/lib/libmc_user.so
fsync /jci/lib/libmc_user.so
mount -o remount,ro /
reboot
```

หรือใช้โปรแกรม WinSCP นำไฟล์จากภายนอกเข้าCMU ดูตัวอย่างที่ [Youtube ลิงค์](https://www.youtube.com/watch?v=tiAQMdtvaT8)

**Q: วิธีเปิด Terminal**
>A :

>**เครื่องมือ**
>- PC USB Keyboard
>
>**เปิด Terminal Console**
>
>1. กดปุ่ม Music+Back+Push ประมาณ 3วินาที
>2. กดปุ่ม Del ค้างไว้ 20วินาที
>3. กดเลข 11 แล้ว Enter
>4. กด Next
>5. เลือก TERMINAL

<a href="http://www.youtube.com/watch?feature=player_embedded&v=M-iJLuxwfzU" target="_blank"><img src="http://img.youtube.com/vi/M-iJLuxwfzU/0.jpg" alt="JCI TEST" width="240" height="180" border="10" /></a>


**Q: ใช้ SSH กับ 56.00.513C ไม่ได้ ?**
>A : ติดตั้งปลั๊กอิน [T] Install SSH_bringback

**Q: FW 56.00.513 หน้าจอค้างไม่ได้ติดตั้ง [T]**

>A : จาก [mazda3revolution](http://mazda3revolution.com/forums/2014-2016-mazda-3-skyactiv-audio-electronics/57714-infotainment-project-584.html)
> ใช้โปรแกรม notepad++ ตั้งชื่อไฟล์ `autorun`ภายในไฟล์เขียนคำสั่งตามด้านล่าง และบันทึกเป็นไฟล์ชนิด UNIX ด้วยเมนู Edit->EOL Conversion -> Unix(LF) แล้วจึงกด save บันทึกไฟล์ใน SDCard แล้วนำ SDCard ไปใส่ช่องที่รถ

* autorun

```
#### autorun ####
#!/bin/sh
echo 1 > /sys/class/gpio/Watchdog\ Disable/value
mount -o rw,remount /
sleep 40
if [ -e /mnt/sd_nav/run.sh]; then
 sh /mnt/sd_nav/run.sh
fi 
```

>ล๊อคอินเข้า CMU ด้วยวิธี SSH หรือ Console แล้วป้อนคำสั่งต่อไป

```sh
cp -a /mnt/sd_nav/autorun /tmp/mnt/data_persist/dev/bin/
chmod +x /tmp/mnt/data_persist/dev/bin/autorun
```

>เสร็จแล้วพิมพ์คำสั่ง

```sh
reboot
```

>หลังจากรีบูทระบบจะทำคำสั่ง `autorun` มีคำสั่งหยุดการทำงาน Watchdog และทำให้เขียนไฟล์ได้ เมื่อบูทเสร็จใช้ SSH กู้ไฟล์ตามวิธีปกติ เมื่อกู้ไฟล์เสร็จแล้วใช้คำสั่งต่อไปนี้ลบไฟล์ออกจากระบบ

```
rm /tmp/mnt/data_persist/dev/bin/autorun
```

อ่านเพิ่มที่ https://github.com/mzdonline/tutorial/blob/master/chapter/FW-56.00.513-unbrick.md

**Q: ขอเพิ่มคำถามพบบ่อยได้หรือไม่**
> A: ได้ โพสในกระทู้ได้ หรือ pm ก็ได้ครับ
