#ภาพตัวอย่าง AIO (บางส่วน)
- [D] Remove the blank album art frame
<a href="https://github.com/Siutsch/AIO---All-in-one-tweaks/blob/master/choose/config_all/blank-album-art-frame/jci/gui/common/images/no_artwork_icon.png"><br /><img src="https://github.com/Siutsch/AIO---All-in-one-tweaks/blob/master/choose/config_all/blank-album-art-frame/jci/gui/common/images/no_artwork_icon.png" width="180" alt="no_artwork_icon.png" ></a> <a href="https://github.com/Siutsch/AIO---All-in-one-tweaks/blob/master/choose/config_all/blank-album-art-frame/jci/gui/common/images/no_artwork_icon2.png"><img src="https://github.com/Siutsch/AIO---All-in-one-tweaks/blob/master/choose/config_all/blank-album-art-frame/jci/gui/common/images/no_artwork_icon2.png" width="180" alt="no_artwork_icon2.png" ></a> <a href="https://github.com/Siutsch/AIO---All-in-one-tweaks/blob/master/choose/config_all/blank-album-art-frame/jci/gui/common/images/no_artwork_icon3.png"><img src="https://github.com/Siutsch/AIO---All-in-one-tweaks/blob/master/choose/config_all/blank-album-art-frame/jci/gui/common/images/no_artwork_icon3.png" width="180" alt="no_artwork_icon3.png" ></a> 

- [F] Speedometer, Media Player, Reboot
<a href="https://github.com/Siutsch/AIO---All-in-one-tweaks/blob/master/choose/help/mzd_SpeedoCompass_kmh_4.3_by_diginix.gif?raw=true"><br /><img src="https://github.com/Siutsch/AIO---All-in-one-tweaks/blob/master/choose/help/mzd_SpeedoCompass_kmh_4.3_by_diginix.gif?raw=true" width="360" ></a> 

- [G]  Date_to_statusbar
<a href="https://raw.githubusercontent.com/Siutsch/AIO---All-in-one-tweaks/master/choose/help/mzd_datum_icons_all.jpg"><br /><img src="https://raw.githubusercontent.com/Siutsch/AIO---All-in-one-tweaks/master/choose/help/mzd_datum_icons_all.jpg" width="360" > </a>

- [H] Custom_color_scheme (CarOS)
<a href="https://github.com/Siutsch/AIO---All-in-one-tweaks/blob/master/choose/help/carOS.png?raw=true"><br /><img src="https://github.com/Siutsch/AIO---All-in-one-tweaks/blob/master/choose/help/carOS.png?raw=true" width="360" ></a>

- [R] Android Auto
<a href="url"><br /><img src="https://github.com/Siutsch/AIO---All-in-one-tweaks/blob/master/choose/help/AndroidAuto.jpg?raw=true" width="360" ></a> 

- [Z] No more background for buttons
<a href="https://github.com/Siutsch/AIO---All-in-one-tweaks/blob/master/choose/help/NoButtonBackground.jpg?raw=true"><br /><img src="https://github.com/Siutsch/AIO---All-in-one-tweaks/blob/master/choose/help/NoButtonBackground.jpg?raw=true" width="360" ></a> 

- [4] Video player cr:[Benato](http://www.mazda2thailand.com/index.php/topic,2786.msg97192.html#msg97192)
<a href="http://www.mazda2thailand.com/index.php?action=dlattach;topic=2786.0;attach=10168;image"><br /><img src="http://www.mazda2thailand.com/index.php?action=dlattach;topic=2786.0;attach=10168;image" width="360" ></a>

- [5] Fuel Consumption Tweak
<a href="https://github.com/Siutsch/AIO---All-in-one-tweaks/blob/master/choose/help/FuelConsumtionTweak.jpg"><br /><img src="https://github.com/Siutsch/AIO---All-in-one-tweaks/blob/master/choose/help/FuelConsumtionTweak.jpg" width="360" ></a>
