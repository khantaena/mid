#ทดสอบไฟล์เสียงชนิด FLAC กับเฟิร์มแวร์: 56.00.240B-ADR
`22ตค2559 `ใช้ปลั๊กอิน [N] กับเฟิร์มแวร์ 56.00.240B-ADR พบว่าใช้งานได้ ดังนั้นวิธีนี้ควรใช้ได้กับ 56.00.100A-ADR และ 56.00.230A-ADR เช่นกัน

**สำคัญ** ถอนติดตั้ง [R] Android Auto ก่อนทำ Factory Reset 

ตนเองชอบทดลองซอฟต์แวร์(เป็นDIYerแนวติดตั้งซอฟต์แวร์ ไม่ถนัดเขียนโปรแกรมเอง) ไม่ได้ให้ความสำคัญกับคุณภาพเสียงมากนัก สนใจ flac เพราะ MZD เปิดไม่ได้ทั้งที่ควรจะทำได้ จากที่เคยทดลองปลั๊กอิน [N] ใน ([#346](http://www.mazda2thailand.com/index.php/topic,2786.msg95048.html#msg95048)) คราวนั้นยังทำไม่ได้ ครั้งนี้ทำงานได้แล้ว โดยใช้ AIO-1.51 Test+Factory Reset นอกจากนี้ได้พยายามค้นหาว่ามีอะไรที่เปลี่ยนไป ได้ตรวจสอบ libmc_user.so พบว่าเป็นไฟล์เดิมนับจาก AIO-1.45 แต่ AIO เวอร์ชั่นนี้ใช้งานได้กับ 56.00.511A-EU ซึ่งเป็นไฟล์เดียวกับ 56.00.513C-ADR แปลว่า 513C น่าจะใช้ [N] ได้(รอการยืนยัน) 
 
เท่ากับว่าเป็นไฟล์เดิมที่เคยทดสอบแล้วพบปัญหา USBใช้ไม่ได้ แต่ครั้งนี้ทดลอง Factory Reset แล้วจึงติดตั้งปลั๊กอิน [N] พบว่า USB ทำงานได้ปกติและอ่านไฟล์ flac ได้ 

**ปลั๊กอินที่มีก่อนติดตั้ง**

- [A] Touch screen
- [G] date-to-statusbar (V2.1)
- [R] Android Auto (แก้ไฟล์ input_filter เองเพื่อให้ Factory Reset ได้ ผมเก็บโค๊ดใน [mazda-connector](httlab//gitlab.com/khantaena/mazda-connector]mazda-connector)
- [4] Video Player 2

หลังเลือก Factory Reset แล้วปลั๊กอินทั้งหมดยังอยู่และทำงานได้เหมือนเดิม ยกเว้น [A] 

ไฟล์ติดตั้งสร้างจาก [AIO-1.51-Test] เลือก [N] อย่างเดียว หรือใช้ไฟล์ที่แปลงจาก AIO ได้ที่ [mzd-plugins](https://gitlab.com/khantaena/mzd-plugins) ติดตั้งใช้ flac/installer และ ถอนติดตั้งใช้ flac/uninstaller โดย copy ลง USB ได้เลย ไฟล์ทั้งหมดได้จาก AIO ทั้งหมดครับเจตนาเพื่อเพิ่มความสะดวกจากลดขั้นตอนการสร้างไฟล์ 
ส่วนแตกต่างจากครั้งที่แล้วคือ Factory Reset หลังจากนั้นติดตั้ง [N] 

**คำเตือน**

1. ก่อนทดสอบขอให้ลองใช้ SSH ไปที่รถเผื่อแผนสำรองกรณีมีปัญหา 
2. เผื่อใจหากพบปัญหา ซึ่งท่านต้องแก้ไขด้วยตนเอง
3. ไฟล์ดังกล่าวเป็นไฟล์ที่ผมทดสอบกับรถตนเองแล้วทำงานได้ ไม่ได้หมายความว่าจะใช้กับรถท่านได้ 

ไฟล์เสียงทั้ง 3ได้ทดสอบฟังจากคอมพิวเตอร์ได้ยินเสียงปกติ ไฟล์ BIS1536-001 ให้เสียงเดียวกัน เพื่อไม่ให้การทดลองสับสนจึงใช้วิธีcopyลงUSBและทดสอบครั้งละไฟล์ 
ผลทดสอบในตาราง มีเพียงไฟล์ 2L-11 stereo-352k-24b15ที่เปิดไม่ได้ครับ 

| Filename                       | Codec | sample-rate | bits-per-sample | result |
|--------------------------------|-------|-------------|-----------------|--------|
| [BIS1536-001-flac_16.flac](http://www.eclassical.com/custom/eclassical/files/BIS1536-001-flac_16.flac)      | FLAC  | 44.1kHz     | 16              | Pass   |
| [BIS1536-001-flac_24.flac](http://www.eclassical.com/custom/eclassical/files/BIS1536-001-flac_24.flac)       | FLAC  | 44.1kHz     | 24              | Pass   |
| [2L-111_stereo-352k-24b_15.flac](http://www.lindberg.no/hires/test/2L-038_stereo_FLAC_352k_24b_01.flac)  | FLAC  | 352.8kHz    | 24              | No     |

# ภาพประกอบ

* รายการไฟล์เพลง

![](https://gitlab.com/khantaena/mid/raw/42c0f6cc08e739302afdd2f5f86f0bc8b49dda41/flac/flac-list.png)

* ไฟล์ 16bit

![](https://gitlab.com/khantaena/mid/raw/42c0f6cc08e739302afdd2f5f86f0bc8b49dda41/flac/flac-24bit.png)

* ไฟล์ 24bit

![](https://gitlab.com/khantaena/mid/raw/42c0f6cc08e739302afdd2f5f86f0bc8b49dda41/flac/flac-24bit.png)