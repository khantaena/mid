#### [ README](https://github.com/Siutsch/AIO---All-in-one-tweaks/tree/Test/README.md "RADME")

# Changelog:
## AIO-1.51-Test น่าจะเป็นเวอร์ชั่นสุดท้ายของ AIO ก่อนย้ายไป [MZD-AIO-TI](https://github.com/Trevelopment/MZD-AIO-TI "MZD-AIO-TI") ซึ่งคาดว่าจะเปิดให้ดาวน์โหลดประมาณปลายปีนี้
### V1.51-Test - 2016-12-12
_**CHANGES/NEWS:**_
- อัพเดท Android Auto เวอร์ชั่น 0.94B ทำให้ AA ใช้ได้อีกครั้ง
- Bugfix for Android Auto from user khantaena : Solved problem from factory reset(you have to change "button_patch=yes" in file choose\15i.txt (line 8) )
- <s>ควบคุม AA ผ่าน Command Knob (ปุ่ม rotary)</s>
- ใช้งานร่วมกับ Google Play Services เวอร์ชั่นใหม่ 
- กำหนดค่าปกติเมื่อติดตั้ง [R] เป็นยกเลิกคุณสมบัติควบคุมผ่านแผงบนพวงมาลัย 
- ส่งผลให้ควบคุม AA ผ่าน touchscreen ได้เพียงทางเดียว หรือใช้คำสั่งเสียง "OK Google" ใน Android Auto 2.0
- ถ้าต้องการใช้งานแผงควบคุมบนพวงมาลัย<s>และ Command Knob</s> แก้ไขไฟล์ choose\15i.txt เปลี่ยนจาก button_patch=no เป็น button_patch=yes (บรรทัดที่ 8)
- เพิ่มการรองรับเฟิร์มแวร์ใหม่ 59.00.326A-ADR
- มีเพียง Only speecam-patch และ track-order/FLAC ที่ยังใช้ไม่ได้ในเวอร์ชั่นนี้
- แก้ไข uninstaller ปลั๊กอิน list_loop (thanks to elior77 from mazda3revolution.com)
- รองรับ FW 56.00.521A-NA จาก FIAT Spider 


### V1.50 - 2016-07-14
- ทดลองใช้ aria2( https://aria2.github.io ) ช่วยอัดเดท AIO 
 - ทำงานอย่างไร : โปรแกรมดาวน์โหลดไฟล์ 'AIO_update.txt' จาก google drive แล้วตรวจการอัพเดท หากพบจะเริ่มดาวน์โหลด zip file และคลายไฟล์อัตโนมัติ 
 - คาดว่าจะใช้ได้ใน AIO 1.51 
- เปลี่ยนสี AIO
- นอกจากเลือกไฟล์ background ด้วยตนเองแล้ว เลือก1ไฟล์จาก80ไฟล์เป็น background ได้ ไฟล์อยู่ใน choose\more_background-images เพียงดับเบิ้ลคลิกที่ไฟล์นั้น (เสียดายที่ไม่มีตัวอย่างไฟล์แสดงผลบนหน้าจอ)
- Background Rotator ใช้ได้แล้ว ทำภาพผ่านเว็บ http://old.photojoiner.net/
- อัพเดท speedometer 4.4  
 - ใช้ความเร็วรถ แทนความเร็ว GPS
 - มีเลขรอบเครื่องยนต์แล้วในตารางขวามือ 
 - เปลี่ยน statusbar สลับเป็น ความสูง/ทิศทาง เมื่อไม่อยู่หน้าจอ speedometer
- รองรับ 56.00.513C-ADR ใน  speedcam-patch (คิดว่าใช้ได้ รอยืนยัน)
- แก้ปัญหา installer,uninstaller ปลั๊กอิน speedcam-patch ไฟล์ data.zip สำหรับเฟิร์มแวร์ 56.00.100A-ADR, 56.00.230A-ADR และ 56.00.401A-JP
 - ใช้ได้กับเฟิร์มแวร์ 58.00.250A-AN 
- อัพเดท videoplay 2.5 อ่านการปรับปรุงในไฟล์ README ที่ 'List of tweaks'-'Video player v2' -'Changes by vic_blam85'

### 1.49 - 2016-06-30
- คำถามที่พบบ่อย AIO_FAQ.txt
- อัพเดทโปรแกรม [F]speedometer, [R]Android Auto และ [4]videoplayer มีการตรวจสอบโปรแกรมที่ใช้ร่วมกัน
- อัพเดทโปรแกรมติดตั้ง [V] swapfile for media player v2 
- [color=green][b]ปลั๊กอินใหม่[/b][/color] [6] ปลี่ยน background อัตโนมัติ (Background Rotator)
  - total time ใช้กำหนดเวลาเวลาแสดงผลหน้าจอทั้งหมด เช่นมี 10รูป total time=600s [total time /10] ดังนั้น Background = 60 วินาทีต่อรูป 
- อัพเกรด Speedometer 4.3
- ยกเลิก background speedometer ไปใช้ background หลัก
- อัพเดท km/L

### 1.48 - 2016-06-16
 - Android Auto 0.92B รองรับโทรศัพท์ OPPO
 - ซ่อนเมนูติดตั้ง Android Auto แต่ยังติดตั้งได้ทางการพิมพ์ R สาเหตุจากปัญหา Factory Reset
 - อัพเดท speedometer 4.2 (problems with new trip distance to 20 meter accuracy, only worked for first 10 km)
 - แก้บั๊ค speedcam-patch (เฉพาะที่ติดตั้ง AIO 1.47)

### 1.47 - 2016-06-01
-ปรับปรุง video player เล็กน้อย by vic_bam85
-pause_on_mute, media order patch, speedcam-patch ใช้ได้แล้วกับ 56.00.240-ADR
- Speedometer 4.2
- แก้บั๊ค Android Auto 0.91B
  - สลับหน้าจอเมื่อเข้าเกียร์ R และสลับกลับ
  - สั่งงานเสียงภาษาไทย(Google Now)ได้จากปุ่ม Talk ที่พวงมาลัย เมื่อใช้ AA
  - กดปุ่ม Home ปิดโปรแกรม

### 1.46 - 2016-05-04
- แก้บัค media order, FLAC
- New version of videoplayer (2.4) by vic_bam85

### 1.45b - 2016-05-02
- แยก VDO Player ออกจาก Speedometer ได้ 
- รองรับ VDO mp4, avi, wmv, flv 
- ปลั๊กอินอ่าน CID SDCard-NAV
- แสดงผล KM/L 
- bugfix speedometer


### V1.45 - 2016- 04- 30
_**BUGFIXES:**_
- Help window was allways fixed in front (if readme was opened, it was allways fixed behind help window). Changed that.
- Text for 'no background behind buttons' tweak wasn't translated to other languages (was only german)
- Speedcam.txt will no longer be copied to NAV SD card, if no compatible FW is detected (NAV App could not be started after that)

_**CHANGES/NEWS:**_
- Modified version of 'bigger album art tweak' by epadillac added (moved picture some pixels to the left)
- Add Polish translation thanks to user sauron2003 from mazda3revolution board (thank you very much!)
- Seperated speedometer and videoplayer (videoplayer now selectable with "4"):
  - Now all 3 tweaks that are available in app menu (speedometer, videoplayer and Android Auto) can be installed / deinstalled individually
- Updated Video player v2 with changes from vic_bam85 (thanks for that)
  - Now can play mp4, avi, wmv, flv
- New version of speedometer 4.1 (now spanish and polish (new) is selectable)
  - Changed umount / mount times for 56.00.511/512/513 (now 55 / 25 sec.), please test, if speedometer works now with this FW versions
- Media- order / FLAC support will be installed now for 56.00.513B- EU and 55.00.760A- NA too
- Pause_on_mute will be installed now for 56.00.513B- EU and 55.00.760A- NA too
- SSH_bringback will be installed now for 56.00.513B- EU too (should work, but could not be tested!)
- Speedcam- patch will be installed now for 56.00.513B- EU and 55.00.760A- NA too (thanks @modfreakz for updating JCI_NNG Tool)
- For speedcam- patch now only newest version from 56.00.513B- EU will be used for 56.00.230A/511A/512A- EU too and from 55.00.760A- NA for 55.00.753A- NA too.
  - I tested this with .230, and files from .513 are working too, I hope this is the same with NA FW. So users with older FW versions can benefit from newest NAV App changes.
- Added blue color scheme for NAV App (thanks to JinZ, who gave us nng content from his Toyota Scion iA)
  - Will be automatically installed/uninstalled, if blue color scheme and/or speedcam- patch is or will be installed/uninstalled.
  - Look here:
  - http://mazda3revolution.com/forums/2014-2016-mazda-3-skyactiv-audio-electronics/122458-aio-all-one-tweaks-55.html#post1696530
  - http://mazda3revolution.com/forums/2014-2016-mazda-3-skyactiv-audio-electronics/122458-aio-all-one-tweaks-56.html#post1697626
- Modified 'blank- album- art- frame tweak', now with picture of a radio, if no album art is found (thanks to craigh for this hint)
  - This works not for DAB (Digital Audio Broadcasting) mode, only for FM mode and for USB mode if there is no cover within MP3 tag
- Added SafetyText_UK_English for removing the safety warning label from the reverse camera
- Added new tweak FuelConsumptionTweak (add fuel efficiency unit KM/L), selectable with "5" (by edyvsr from mazdateammexico.com)
  - Therefore changed 'ready to copy' from '5' to '0' (only '6' left ...)
- Combined 'bigger album art tweak' with mods from 'blank- album- art- frame', otherwise there's a black frame inside the bigger album art cover (thanks to cynric for this hint)
  - You can still install 'blank- album- art- frame' individually, but if you uninstall it, if 'bigger album art tweak' is still installed, the black frame will return. In this case, install 'bigger album art tweak' again.


### V1.44 - 2016- 04- 13
_**BUGFIXES:**_
- Fixed uninstaller for custom color schemes. Color of speedometer was not change to red too
- Problem with manual switching language fixed
- Problem with rare non- functional USB ports seems to have been due to faulty files of libmc_user.so while installing/uninstalling media order /FLAC support patch
  - Therefore rewrote installer/uninstaller with additional md5 check in order to prevent the problem for the future (hopefully). Apologies to all who had unfortunately made it! :(
- Problem with main_menu_loop installer fixed, had deleted the MainMenuCtrl.js file by mistake (thanks to jdurrego).

_**CHANGES/NEWS:**_
- New Speedometer 4.0, now available in app menu! (thank you Diginix)
  - Speedometer and Videoplayer will be installed only together, no more selection atm
  - Automatic umount / mount for NAV SD card if FW .511 / .512 is installed, but it seems not to be working
- Update of "Android Auto" Headunit App to V0.83
- New scheme 'carOS' by epadillac (thank you)
- Red color scheme (original) is now available in installation mode too
- Bigger album art tweak by epadillac (thank you)
- No buttons background graphics tweak by epadillac (thank you)
- Enable Wifi for NA FW- versions
- Since I am running again out of letters: changed 'ready to copy' from 'Z' to '5'
  - No more letters left - no more new tweaks! :(


### V1.43 - 2016- 03- 24
_**BUGFIXES:**_
- Problem with installation of SSH_bringback for 56.00.512A- EU fixed
- Order of installation of background image changed, so that an own image not will be overwritten again, if installed together with custom infotainment color tweak
- Fixed uninstaller for Android Auto: an importand file was deleted too (/usr/lib/gstreamer- 0.10/libgstalsa.so), so playing files from USB drive wasn't working anymore.
- Fixed speedcam- patch installer, FW 55.00.753 was not recognized, so this tweak was not installed.
- Fixed media- order/FLAC support tweak installer, FW 55.00.753 was not recognized, so this tweak not installed.
- Fixed pause_on_mute installer, FW 55.00.753 was not recognized, so this tweak was not installed.

_**CHANGES/NEWS:**_
- Could save some space, because patch of file 'jci- linux_imx6_volans- release' for speedcam- patch is now done directly with JCI_NNG_Tool tool (thankz to Modfreakz for that!)
- You can now choose, if you want to copy a speedcam.txt file during installation of speedcam- patch, or not.
  - this could be useful, if you only want to update the speedcam- patch, but not change your existing speedcam.txt, or want to copy your own one.
- Now CID of SD card is read automatically and stored to root of USB drive and SD- card as 'SD_CID.txt' if SD card is present (thanks to Modfreakz for this).
- Update of "Android Auto" Headunit App to V0.81

_**TODO**_:
- Sill waiting for speedometer and media player v2 to be in native app menu (I don't give up ;) )


### V1.42 - 2016- 03- 20  -  again, a lot! :)
_**BUGFIXES:**_
- Problem with installation of blank- album- art- frame finally fixed now (thanks to Falko). The graphic files were overwritten again by installation of custom color schemes.
- If custom color scheme and background image both were selected, the background image was overwritten from the one from color scheme. This is now corrected. (thanks to Intruder)

_**CHANGES/NEWS:**_
- New Speedomter V 3.7 with media player v2 (if used with small speedo date_to_statusbar mod V2.2 will be selected automatically too!)
  - Only for 56.00.100 ADR, 56.00.230A EU and 55.00.753A NA, with 56.00.511A/512A EU there are no GPS data anymore! :(
- New date_to_statusbar mod v2.2 with numeric date, increase icon size (Wifi, Bluetooth...), disabled red border of system messages and smaller font size for all statusbar texts
  - You can choose from v1.0, v2.1 and new v2.2
- Update of "Android Auto" Headunit App to V0.7
- New tweak to remove message reply text "Sent from my Mazda Quick Text System" for all languages.
- Media- order / FLAC support now for 56.00.100 ADR and 56.00.753 NA (same files as .230) and 56.00.511/.512 EU (same files) available too (thanks to diorcety for patching)
- Pause_on_mute will be installed now for 56.00.100 ADR and 55.00.753 NA (same files as .230) and 56.00.511/.512 EU (same files) too.
- Speedcam- patch will be installed now for 56.00.100 ADR and 55.00.753 NA (same files as .230) and 56.00.511/.512 EU (same files) too (thanks Diginix for patching all new files)
- Make swapfile on USB drive for media player v2. You have to do this on your USB drive that contains music and/or movies which remains in car!
- New version of castscreen- receiver 2016- 03- 08
- Added "peep" when copying of selected tweaks is finished and if copying to USB drive is finished.
- Now Screenshots are available for a few tweaks, open help menu (press '9') and click on "open screenshot" below the tweak text in help menu window.
- JCI test console (diagnostic menu) can now be installed/removed as individual tweak (was done automatically with AIO 1.41)
- By pressing '8', the most tweaks will be selected for installation mode, for deinstallation mode all, except backup and swapfile.
  - Remember: by pressing '1' or '2' to switch to installation / deinstallation mode, all the markers are cleared and you can start over. 
- New tweak for disable/enable the boot animation to red button menu
- Since I run out of letters: changed help from 'X' to '9' and switching language from 'Y' to '7'


### V1.41 - 2016- 02- 27
_**BUGFIXES:**_
- Found bug if switching (back) to installation mode by pressing "1", some variables were not set, so no config folder was created. Sorry!
- It seems, that the mount point for USB drives isn't allways for example /mnt/sda1/, but /mnt/sda/ only too. So changed dataRetrieval_config.txt to /mnt/sd*/.

_**CHANGES:**_
- Installation now also possible with 56.00.511 with automatic detection during installation, right version for speedcam- patch and pause_on_mute is used.
- Date_to_statusbar mod now with selectable older version v1.0 and new v2.1

_**NEWS:**_
- Added SSH_bringback for 56.00.511, but will be only installed, if .511 is detected and not already installed. No Deinstaller available!
- Speecam- patch now for 56.00.511 too (only .230 and .511, will be detected automatically during installation)
- Pause_on_mute now for 56.00.511 too (only .230 and .511, will be detected automatically during installation)
- FLAC support (together with media order patch)
- "Android Auto" Headunit App V0.3 (successfully tested with my own LG G4 and Samsung S4)
- JCI test console (diagnostic menu) can now be opened by pressing the clock in the upper right corner of display settings menu for one second (tweak from mzd3- k (Sumire Racing))

### V1.40 - 2016- 02- 20  -  It has taken some time, but there has done a lot, so jump from 1.36 to 1.40 ;)

_**BUGFIXES:**_
- Repaired incorrect 'speedcam_Deutschland_ohneMobileBlitzer.txt' (used semicolon as delimiter instead of a comma), so it could not be imported
- Fixed problem with speedcam uninstaller: speedcam.txt and speedcam.spdb were not automatically deleted from NAV SD card.
  - maybe the reason, why NAV App could not be started after removing tweak, speedcam.txt and speedcam.spdb may no longer be present without the patch.

_**CHANGES:**_
- New installation routine for "no- more- disclaimer" and "change order of the audio source list".
  Everything can now be installed seperately without even removing the other tweak.
  - this though works only if both were installed once each with the new AIO V1.37 and higher (a file marker for detection of installation is set)!
- New installation routine for custom- infotainment- colors and speedometer. Both can now be installed individually, but the color is adjusted anyway:
  - If speedometer was already installed, then color will adjusted anyway, if custom- infotainment- colors will be installed without speedometer again.
  - If custom- infotainment- colors was already installed (with >= 1.37), then color of speedometer will adjusted anyway, if speedometer will be installed without custom- infotainment- colors.
  - this though works only if the installation of custom- infotainment- colors is done with AIO V1.37 and higher (a file marker for detection of installation will set)!
- New installation routine for "list_loop_mod", can now be installed with or without additional shorter_delay_mod (which generates frequent beeps!)
- Each speed in all speedcam files increased by 5 kmh (you should be annoyed less now)
- Changed the language detection a little. I hope it works now. If not, send me first and possibly second line of Window (i.e. Detected CountryCode is xx, which is unknown).
- Every command is logged to AIO_log.txt for troubleshooting (look at root of USB drive after applying / removing of tweaks).
- In the beginning it is tested whether mount point of USB drive really works (try to copy a file from MZD to USB drive). If not, system will reboot automatically.
- Popup window of detected MZD version before start of installation / removing, with the possibility to still manually cancel at this point (you have to press "OK" to start).
- Every selected tweak generates a popup window on MZD System now during installing / removing, so you know exactly at what point it is and how long it takes.
- To get AIO working in simulation, the language folder "01" for english is renamed to "1" and code in choose.cmd was adjusted accordingly, so it should work now.

_**NEWS:**_
- New Speedometer 3.6 with permanent speed + direction in the status bar
  - Support for 'no nav SD' is missing in V3.6, the compass fix needs some changes to fits to 3.6 code, so it's coming again for V4.0
- Added 8 more images for removing the safety warning label from the reverse camera (french, czech, danish, italian, spanish, dutch, polish, russian)
- Translation for Spanish created (with google translator, so sorry to the native Spanish speaking people!) for testing the new manual language switching.
- Added help- system (press 'x' to open), only german and english at this time. Text suggestions welcome! :)
- AND FINALLY: rewrote choose.cmd from scratch. No more interview (but also no more funny answers),
  - only one page to select all tweaks for installation and deinstallation and manual switching the language (if you want and if already translated).
  - Please only start locally, is very slow from a network connection!

_**TODO**_:
- "Android Auto" Headunit App installer get to running
- Waiting for Speedometer 4.0 with integration in native app menu (hopefully coming soon, thanks to Herko- ter- Horst!).
- Prepare for additional installation with 56.00.511A (adjustments necessary only for speecam- patch, media- order_patching and pause_on_mute),
perhaps then with automatic detection during installation.


### V1.36 - 2016- 02- 07
- Added additional popup window at beginning of installation / deinstallation
- Added automatic reboot at end of scripts (you are asked for it)
- Problem with position 3 "Installer with full backup" fixed - code for full backup was not copied to tweaks.sh - sorry!
- Changed text colors to something slightly lighter for better reading
- Added some more new background images to \choose\more_background- images (thanks to grandiotk!)
- Added new tweak "castscreen- receiver" (thanks to trookam for USB script!)
- Forgot CountryCode "43" for Austria


### V1.35 - 2016- 02- 05
- "- a" added to most copy commands, seems to be importand if overwriting files (file rights are maintained)
- Color of speedometer graphics will be changed to red again too, if color scheme uninstaller is used
- First line (shebang) changed from "#! /bin/sh" (with space) to "#!/bin/sh" (without space), don't really know, if it's necessary, I think both works
- Changed mount point für USB drive from "/mnt/sd*/" or "/mnt/sd?1/" to for- next loop for detection of correct mount- point for USB drive
- Every CountryCode > 50 will get english language (better than no text :) )


### V1.34 - 2016- 01- 31
- Problem with color scheme uninstaller fixed, should go back to red now
- Problem with uninstaller for semi- transparent parking sensor graphics fixed, should work now
- Problem with uninstaller for restore_audio_order_and_disclaimer fixed, should work now
- New folder "_copy_content_to_root_of_fat32_usb_stick" for easier manual copying to USB drive (thanks for tip to remizik!)
- Prepared for more languages, english and german strings outsourced in files, more languages now possible!


### V1.33 - 2016- 01- 29
- Copy only selected tweaks to config (for installation) or config_org (for deinstallation) folder, copying to SD Card should be much faster now depending on what you've selected
- Additional question whether a full backup of MZD System will be copied to USB drive before applying the tweaks
- Problem of sometimes not deleted fps.js fixed (FPS was counter shown in the corner)
- Problem of date- to- statusbar uninstaller fixed, should work now
- Automatic detection of correct compass installation (if no valid NAV SD Card is inserted during installation of speedometer, it will change necessary file)
- Copy of files to USB drive can be done after selection of tweaks


### V1.32 - 2016- 01- 27
- Problem with color scheme installer fixed, should work now
- After only hitting return (enter), now the shown tweak will not be installed (same like "n", before it was like "y")
- A summary of the selected tweaks will be displayed after selection for installation and uninstallation part
- Both languages were displayed for english uninstaller - fixed now

### V1.31 - 2016- 01- 26
- New version of "Date_to_statusbar_mod_by_diginix_v2.1"
- New version of "Speedo- Compass- Video_Tweak_v3.5"


### V1.3 - 2016- 01- 25
- Too many bugfixes :(
- Text output in german language too with automatic language detection for german, otherwise english
- README_All- in- one_tweaks.txt updated with german descriptions too
- Officially released


### V1.2x - 2016- 01- 20
- Create 'choose.cmd' to generate individual tweaks.sh file
- Bugfixes after testing by some daring users! ;)


### V1.1x - 2016- 01
- Some own testing and bugfixes
- Create README_All- in- one_tweaks.txt


### V1.0 - 2016- 01
- Only put together all tweaks in one folder to copy at once for myself
